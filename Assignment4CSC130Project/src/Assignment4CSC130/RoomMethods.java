package Assignment4CSC130;
/**
 For use with Rooms Class.
 */
public class RoomMethods
{
	private String walls = "white";
	private String floor = "Carpet";
	private int windows = 0; 
/**
*Default room when no arguments are entered.
*@param none entered
*@return White walls, Carpeted flooring, and 0 windows 
*/
	public void room()
	{		
		System.out.println ("The default room attributes are: ");
		System.out.println ("Walls: " + walls);
		System.out.println ("Flooring: " + floor);
		System.out.println ("Windows: " + windows);		
	}
/**
 * Allows input of wall color, flooring type and number of windows.
 * @param walls color
 * @param floor type
 * @param windows number of
 */
	public void room(String walls, String floor, int windows)
	{
		this.walls = walls;
		this.floor = floor;
		this.windows = windows;
	}
/**
 * When invoked, gives all of the values for the object that were entered in as arguments.
 * @return walls, floor, windows
 */
	public void writeOutput ()
	{
		System.out.println ("The room attributes are: ");
		System.out.println ("Walls: " + walls);
		System.out.println ("Flooring: " + floor);
		System.out.println ("Windows: " + windows +"\n");
	} 
/**
 * For use when new arguments need to be entered.
 * @param newWalls
 * @param newFloor
 * @param newWindows
 */
	public void setRoom(String newWalls, String newFloor, int newWindows)
	{
		if (newWindows >= 0)
			windows = newWindows;
		else
		{
			System.out.println("Error: you can not have negative windows.");
			System.exit(0);
		}
		walls = newWalls;
		floor = newFloor;
	}
/**
 * Accessor method for walls.
 * @return walls
 */
	public String getWalls()
	{
		return walls;
	}
/**
 * Accessor method for floor
 * @return floor
 */
	public String getFloor()
	{
		return floor;
	}
/**
 * Accessor method for windows
 * @return windows
 */
	public int getWindows()
	{
		return windows;
	}
/**
 * toString method to get all values of an object in String format
 * @return walls, floor, windows	 
 */
	public String toString() 
	{
		return "Walls: " + walls + "\nFloors: " + floor + "\nWindows: "+ windows;
	}
}
