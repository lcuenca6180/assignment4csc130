package Assignment4CSC130;

public class Rooms 
{

	public static void main(String[] args) 
	{
		//Here is room1 from the RoomMethods class. The walls are painted yellow, the flooring is hardwood and there is one window.
		RoomMethods room1 = new RoomMethods();
		room1.room("yellow", "hardwood", 1);
		room1.writeOutput();		
		
		
		//Here is room2 from the RoomMethods class. The walls are painted purple, the flooring is tile and there are no windows.
		RoomMethods room2 = new RoomMethods();
		room2.room("purple", "tile", 0);
		room2.writeOutput();	
		
		//Here is room3 from the RoomMethods class. The walls are painted white, the flooring is carpet and there are three windows.
		RoomMethods room3 = new RoomMethods();
		room3.room("white", "carpet", 3);
		room3.writeOutput();

		
		//System.out.println(room3.getWalls());
		//System.out.println(room3.toString());	
	}
}
